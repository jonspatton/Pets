{/*
    A template for components in the React web app.
    @Jon S. Patton
*/}

{/* You need to import React. */}
import React, { Component } from 'react';

{/* This contains the code for how you want the stuff in ths class to display.
    You can put HTML and CSS in here as well.
    
    @return the display details to be implemented by the main app
    */}
class ClassTemplate extends Component {
  render() {
    return (

      {/* Display details */}
      <div className="ClassTemplate">

      </div>
    });
  }
}

{/* Remember to change the name here. This actually "uses" the component you've made */}
export default ClassTemplate;
